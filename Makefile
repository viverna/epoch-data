SHELL=/bin/sh

.PHONY: clean 

clean:

install: 
	mkdir -p $(DESTDIR)/lib/epoch/objects
	cp -f lib/epoch/objects/* $(DESTDIR)/lib/epoch/objects/
uninstall:
	rm -f $(DESTDIR)/lib/epoch/objects/*
	rmdir -p $(DESTDIR)/lib/epoch/objects/
