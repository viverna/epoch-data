EPOCH-DATA
=======

What is it
-------

Epoch is a single-threaded init system for Linux 2.6+, designed to be useful yet
small and unintrusive. It uses a declarative configuration style. For
instructions on creating a configuration file for Epoch, please visit ([Epoch
config page](http://universe2.us/epochconfig.html)). Additional information is
available at ([Epoch main page](http://universe2.us/epoch.html)). Epoch is
public domain software. See UNLICENSE.TXT for more details. Epoch is developed
by Subsentient. See ([github page](https://github.com/Subsentient)) for more
details.

This Git repository make debian package for Devuan. This package install a set
of config file for epoch.

